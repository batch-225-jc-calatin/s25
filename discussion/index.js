//[SECTION] JSON Objects
/*
	- JSON stands for Javascript Object Notation.
	- JSON is also used in other programming languages hence the name JavaSCript Object Notation

	Syntax : 

	{
		"propertyA": "valueA",
		"propertyB": "valueB"

	}

*/


// JSON Object

/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// [SECTION] JSON Arrays
/*
"cities": [
    { "city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Manila City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Makati City", "province": "Metro Manila", "country": "Philippines" }
]
*/


// [SECTION] JSON METHODS
// - the JSON object contains methods for parsing and converting data into stringified JSON

// [SECTION] Converting Data Into stringfied JSON
/*
	- Stringfied JSON is a javascript object converted into string to be used in other function of a javacript application.
	- They are commonly used in HTTP request where information is required to be sent and recieved in a stringfied JSON format
	-Request are an important part of programming where application communicates with a backend application to perform different tasks such as getting/creating data in a database.
*/


let batchesArr = [{batchName: 'Batch X'},{batchName: 'Batch Y'}];

// The "stringify" method is used to convert JavaScript objects intop a string.

console.log('Results from stringify method:');
console.log(JSON.stringify(batchesArr));

// Trough Objects


let data = JSON.stringify({

	name: 'John',
	age: 31,
	address: {

		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data);


// [SECTION] Using Stringify Method with Variable

/*
	- Syntax :
	JSON,stringify({
		propertyA: variableA,
		propertyB: variableB
	})

*/

/*
let firstName = prompt('What is your First Name?');
let lastName = prompt('What is your Last Name?');
let age = prompt('What is your Age?');

let address = {
	city: prompt('Which city do you live in?'),
	country : prompt('Which country does your City Address belong?')
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})
console.log(otherData);*/





// [Section] Converting stringified JSON into JavaScript objects
/*
	- Objects are common data types used in applications because of the complex data structures that can be created out of them
	- Information is commonly sent to applications in stringified JSON and then converted back into objects
	- This happens both for sending information to a backend application and sending information back to a frontend application
	-Parsing means analyzing and converting a program into an internal format that a runtime environment can actually run
	- JSON.parse()
*/

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{ "name": "John", "age": "31", "address": { "city": "Manila", "country": "Philippines" } }`

console.log(JSON.parse(stringifiedObject));